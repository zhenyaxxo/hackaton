﻿using System;

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgramController : MonoBehaviour
{
    private List<GameObject> cases;

    public GameObject[] panels;
    public Server server;


    //GameObject's для NewSolutionPanel
    public InputField FIO;
    public Toggle[] gender;
    public InputField age;
    public Dropdown Case;
    public Dropdown Diagnosis;


    public Toggle[] toggles;
    public Button[] buttons;


    public ScrollRect scrollRect;
    public GameObject scrollContent;
    public GameObject prefabScrollContent;


    public void ClearGUID()
    {
        TempInfo.Solution_id = Guid.Empty;
    }
    public void GoToPanel(string panelName)
    {
        for(int i = 0; i < panels.Length; i++)
        {
            if(panels[i].transform.name == panelName)
            {
                panels[i].gameObject.SetActive(true);
            }
            else
            {
                panels[i].gameObject.SetActive(false);
            }
        }
    }

    public void ClearToggle()
    {
        for(int i = 0; i < toggles.Length; i++)
        {
            toggles[i].isOn = false;
        }
    }
    public void CreateNewSolution()
    {
        if (TryToCreate())
        {
            Solutuion solutuion = new Solutuion();
            var userFIO = FIO.text.Split(' ');
            solutuion.id = Guid.NewGuid();
            solutuion.Surname = userFIO[0];
            solutuion.Name = userFIO[1];
            solutuion.MiddleName = userFIO[2];

            //gender
            for (int i = 0; i < gender.Length; i++)
            {
                if (gender[i].isOn)
                {
                    if (i == 0) solutuion.gender = Gender.Man;
                    else solutuion.gender = Gender.Woman;
                }
            }

            solutuion.Age = Convert.ToInt32(age.text);
            int index = Case.value;

            solutuion.Case = Case.options[index].text;
            solutuion.Diagnosis = Diagnosis.value.ToString();
            solutuion.criterions = new System.Collections.Generic.List<Criterion>();
            for(int i = 0; i < 9; i++)
            {
                solutuion.criterions.Add(new Criterion());
            }
            
            server.AddSolution(solutuion);
            TempInfo.Solution_id = solutuion.id;
            GoToPanel("CheckCasePanel");

        }else
        {
            Debug.LogError("Заполните все поля!");
        }
    }

    private bool TryToCreate()
    {
        if (!String.IsNullOrEmpty(FIO.text))
        {
            if(!String.IsNullOrEmpty(age.text))
            {
                return true;
            }
            else
            {
                return false;
            }
        }else
        {
            return false;
        }
    }

    
    //CheckCasePanel functions
    public void OnValueChanged(Button button)
    {
        var data = server.GetDataBase();
        if (button.interactable == true)
        {
            button.interactable = false;
            
            return;
        }else
        {
            button.interactable = true;

            return;
        }

    }

    public void ChangeValue(int index)
    {
        var data = server.GetDataBase();
        if (toggles[index].isOn == true)
        {
            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].id == TempInfo.Solution_id)
                {
                    data[i].criterions[index].Checked = true;
                }
            }
            server.SaveDataBase(data);
            return;
        }
        else
        {
            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].id == TempInfo.Solution_id)
                {
                    data[i].criterions[index].Checked = false;
                }
            }
            server.SaveDataBase(data);
            return;
        }
    }

    public void AddImage(Toggle toggle)
    {
        toggle.interactable = false;
    }

    public void AddImage(int index)
    {
        var data = server.GetDataBase();
        for (int i = 0; i < data.Count; i++)
        {
        if (data[i].id == TempInfo.Solution_id)
                {
                    data[i].criterions[index].AddImage = true;
                }
            }
        server.SaveDataBase(data);
    }

    public void RemoveImage(Toggle toggle)
    {
        toggle.interactable = true;
    }

    public void RemoveImage(int index)
    {
        var data = server.GetDataBase();
        for (int i = 0; i < data.Count; i++)
        {
            if (data[i].id == TempInfo.Solution_id)
            {
                data[i].criterions[index].AddImage = false;
            }
        }
        server.SaveDataBase(data);
    }

    public void SetOnGameObject(GameObject gameObject)
    {
        gameObject.SetActive(true);
    }

    public void SetOffGameObject(GameObject gameObject)
    {
        gameObject.SetActive(false);
    }



    //CurrentCases

    public void GetCurrentCases()
    {
        cases = new List<GameObject>();
        var data = server.GetDataBase();
        for(int i = 0; i < data.Count; i++)
        {
            if(data[i].status == Status.Closed)
            {
                data.RemoveAt(i);
                i--;
            }
        }

        
        

        for(int i = 0; i < data.Count; i++)
        {
            GameObject scrollItemObj = Instantiate(prefabScrollContent);
            scrollItemObj.transform.SetParent(scrollContent.transform, false);
            for(int j = 0; j < 4; j++)
            {
                if (j == 0)
                {
                    var FIOtext = "";
                    FIOtext += data[i].Surname + " ";
                    FIOtext += data[i].Name + " ";
                    FIOtext += data[i].MiddleName;
                    scrollItemObj.transform.GetChild(0).gameObject.transform.GetComponent<Text>().text = "ФИО: " + FIOtext;
                }
                if(j == 1) scrollItemObj.transform.GetChild(1).gameObject.transform.GetComponent<Text>().text = "Возраст: " + data[i].Age + " лет";
                if(j == 2)
                {
                    if(data[i].gender == Gender.Man) scrollItemObj.transform.GetChild(2).gameObject.transform.GetComponent<Text>().text = "Пол: Мужской";
                    else scrollItemObj.transform.GetChild(2).gameObject.transform.GetComponent<Text>().text = "Пол: Женский";
                }
                if(j == 3) scrollItemObj.transform.GetChild(3).gameObject.transform.GetComponent<Text>().text = "Случай: " + data[i].Case;
            }
            scrollItemObj.transform.GetComponent<SpawnContent>().guid = data[i].id;
            cases.Add(scrollItemObj);
        }

        scrollRect.verticalNormalizedPosition = 1;
    }

    public void LoadSolution(Guid guid)
    {
        GoToPanel("CheckCasePanel");
        int index = 0;
        var data = server.GetDataBase();
        for(int i = 0; i < data.Count; i++)
        {
            if(data[i].id == guid)
            {
                index = i;
            }
        }

        ClearToggle();
        for(int i = 0; i < 9; i++)
        {
            if(!data[index].criterions[i].Checked && !data[index].criterions[i].AddImage)
            {
                toggles[i].isOn = false;
                toggles[i].interactable = true;
                buttons[i].gameObject.SetActive(true);
                buttons[i + 9].gameObject.SetActive(false);
            }


            if(data[index].criterions[i].Checked)
            {
                toggles[i].isOn = true;
                buttons[i].gameObject.SetActive(true);
                buttons[i + 9].gameObject.SetActive(false);
            }

            if (data[index].criterions[i].AddImage)
            {
                print("+");
                toggles[i].interactable = false;
                buttons[i].gameObject.SetActive(false);
                buttons[i + 9].gameObject.SetActive(true);
            }
            else
            {
                toggles[i].interactable = true;
                buttons[i].gameObject.SetActive(true);
                buttons[i + 9].gameObject.SetActive(false);
            }
        }

        for(int i = 0; i < cases.Count; i++)
        {
            Destroy(cases[i]);
            cases.RemoveAt(i);
            i--;
        }
    }

    public void ClearPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
    private void OnApplicationQuit()
    {
        ClearGUID();
        PlayerPrefs.Save();
    }
}
