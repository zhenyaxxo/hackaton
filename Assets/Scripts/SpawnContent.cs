﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SpawnContent : MonoBehaviour
{
    public Guid guid;
    private ProgramController programController;
    private void Start()
    {
        programController = GameObject.FindGameObjectWithTag("ProgramController").transform.GetComponent<ProgramController>();

    }

    public void Click()
    {
        TempInfo.Solution_id = guid;
        programController.LoadSolution(guid);
    }
}
