﻿using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
public class DataBase
{
    List<Solutuion> solutions = new List<Solutuion>();

    public List<Solutuion> GetDataBase()
    {
        this.solutions = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Solutuion>>(PlayerPrefs.GetString("SolutionsDataBase"));
        return solutions;
    }

    public void SaveDataBase()
    {
        PlayerPrefs.SetString("SolutionsDataBase", JsonConvert.SerializeObject(solutions.ToArray()));
    }

    public void SaveChangedDataBase(List<Solutuion> solutuions)
    {
        this.solutions = solutuions;
        SaveDataBase();
    }

    public void AddSolution(Solutuion solutuion)
    {
        this.solutions.Add(solutuion);
        SaveDataBase();
    }
}
