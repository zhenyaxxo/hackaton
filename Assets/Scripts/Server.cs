﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class Server : MonoBehaviour
{
    private DataBase dataBase;
    public Button allCaseButton;

    private void Start()
    {
        
        dataBase = new DataBase();
        if (!String.IsNullOrEmpty(PlayerPrefs.GetString("SolutionsDataBase")))
        {
            print(dataBase.GetDataBase());
            allCaseButton.interactable = true;
        }
        else
        {
            allCaseButton.interactable = false;
        }
        print("Server is Start!");
    }

    private void Update()
    {
        if (String.IsNullOrEmpty(PlayerPrefs.GetString("SolutionsDataBase")))
        {
            allCaseButton.interactable = false;
        }else
        {
            allCaseButton.interactable = true;
        }
    }


    public void AddSolution(Solutuion solutuion)
    {
        dataBase.AddSolution(solutuion);
    }

    public List<Solutuion> GetDataBase()
    {
        return dataBase.GetDataBase();
    }

    public void SaveDataBase(List<Solutuion> solutuions)
    {
        dataBase.SaveChangedDataBase(solutuions);
    }

    
}