﻿using System.Collections.Generic;
using System;

public enum Status
{
    Open,
    Closed
}
public enum Gender
{
    Man,
    Woman
}
public class Solutuion
{
    public Guid id;
    public string Name;
    public string Surname;
    public string MiddleName;
    public Gender gender;
    public int Age;
    public string Case;
    public string Diagnosis;
    public List<Criterion> criterions;
    public Status status = Status.Open;
}
